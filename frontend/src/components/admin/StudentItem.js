import React, { Component } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class StudentItem extends Component {
  constructor(props){
    super(props);
    this.state ={isEdit:false, modal: false}
    this.editStudent = this.editStudent.bind(this);
    this.editStudentSubmits = this.editStudentSubmits.bind(this);
    this.deleteStudent = this.deleteStudent.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  editStudent(event){
    event.preventDefault();
    this.setState((prevState,props) => ({
      isEdit : !prevState.isEdit,
    }))
    this.toggle();
  }


  editStudentSubmits(){
    const {id} = this.props.student;
    this.setState((prevState,props) => ({
      isEdit : !prevState.isEdit,
      modal : !this.state.modal
    }));
    const username = this.nameInput.value;
    const password = this.passInput.value;
    const companyname = this.companyInput.value;
    const companyphone = this.companyInput.value;
    const companyemail = this.emmailInput.value;
    const industry = this.industryInput.value;
    const companydescription = this.descriptionInput.value;
    const companyaddress = this.addressInput.value 

    this.props.editStudentSubmit (
      id,
      username,
      password,
      companyname,
      companyphone,
      companyemail,
      industry,
      companydescription,
      companyaddress
    ); 
  }

  deleteStudent(event){
    event.preventDefault();
    const {id} = this.props.student;
    this.props.deleteStudent(id);
  }

  toggle() {
    this.setState(() => ({
      modal : !this.state.modal
    }));
  }

  render() {
    
    const {id, username, companyname, companyphone, companyemail, industry, companydescription, companyaddress, password} = this.props.student;
    return (

        this.state.isEdit === true ? (
          <div>
          <Modal isOpen={this.state.modal}>
          <form >
            <ModalHeader>save</ModalHeader>
            <ModalBody>
                <div className="row">
                  <div className="md-form form-sm col-md-12">
                    <label for="inputSMEx">username:</label>
                    <input className="form-control" id="inputSMEx" ref={nameInput => this.nameInput = nameInput} defaultValue ={username}/>
                  </div>
                </div>
                <div className="row">
                  <div className="md-form form-sm col-md-12">
                    <label for="inputSMEx">password:</label>
                    <input className="form-control" id="inputSMEx" ref={passInput => this.passInput = passInput} defaultValue ={password}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyname:</label>
                    <input className="form-control" defaultValue={companyname} ref={companyInput => this.companyInput = companyInput}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyphone:</label>
                    <input className="form-control" ref={phoneInput => this.phoneInput = phoneInput} defaultValue={companyphone}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyemail:</label>
                    <input className="form-control" ref={emmailInput => this.emmailInput = emmailInput} defaultValue={companyemail}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>industry:</label>
                    <input className="form-control" ref={industryInput => this.industryInput = industryInput} defaultValue={industry}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companydescription:</label>
                    <input className="form-control" ref={descriptionInput => this.descriptionInput = descriptionInput} defaultValue={companydescription}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyaddress:</label>
                    <input className="form-control" ref={addressInput => this.addressInput = addressInput} defaultValue={companyaddress}/>
                  </div>
                </div>
            </ModalBody>
            <ModalFooter>
              <Button class="btn btn-secondary"  onClick={this.editStudentSubmits}>save</Button>
              <Button class="btn btn-secondary" onClick={this.toggle}>Cancel</Button>
             
            </ModalFooter>
            </form>
          </Modal>
           </div>
        ) : (
          <tr key={this.props.index}>
              <td>{id}</td>
              <td>{username}</td>
              {/* <td>{password}</td> */}
              <td>{companyname}</td> 
              <td>{companyphone}</td>
              <td>{companyemail}</td>
              <td>{companydescription}</td>
              <td>{companyaddress}</td>
              <td>{industry}</td>
              <td><a href="/" onClick={this.editStudent}>edit</a> / <a href="/lll" onClick={this.deleteStudent}>delete</a></td>
          </tr>
        )
    );
  }
}