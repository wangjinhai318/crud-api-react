import React, { Component } from 'react';
import StudentItem from './StudentItem.js';
import { Modal } from 'react-bootstrap';
import ModalComponent from './ModalComponent.js';
export default class StudentList extends Component {
  render() {
    let students = this.props.studentList;
    const trItem = students.map((item,index) => (
      <StudentItem
        key={index}
        student={item}
        index={index}
        editStudentSubmit={this.props.editStudentSubmit}
        deleteStudent={this.props.deleteStudent}    
      ></StudentItem>
    ));
    return <tbody>{trItem}</tbody>;
  }
}