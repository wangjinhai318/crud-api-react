import React, { Component } from 'react'

export default class Signout extends Component {
    render() {
        return (
            <div>
                <h1>You have been signed out</h1>
            </div>
        )
    }
}
