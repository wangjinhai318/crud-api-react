import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import { Table, Form, Col, Row, Pagination, } from 'react-bootstrap';
import StudentList from './StudentList.js';
import axios from 'axios';
import ModalComponent from './ModalComponent'

const url = 'https://pegasus-backend.herokuapp.com/admin/';

export default class Candidate extends Component {
  state = {
    isLoading: true,
    users: [],
    error: null
  };

  

  fetchUsers() {
    fetch(url + `getallemployers`)
      .then(response => response.json())
      .then(data =>
        this.setState({
          users: data,
          isLoading: false,
        })
      )
      .catch(error => this.setState({ error, isLoading: false }));
  }

  editStudentSubmit = this.editStudentSubmit.bind(this);
  deleteStudent = this.deleteStudent.bind(this);
  addStudentSubmit = this.addStudentSubmit.bind(this);

  componentDidMount() {
    this.fetchUsers();
  }

  addStudentSubmit (username, password, companyname, companyphone, companyemail, industry, companydescription,companyaddress, usertype) {
      axios.post('https://pegasus-backend.herokuapp.com/createuser', {
        "username": username,
        "password": password,
        "companyname": companyname,
        "companyphone": companyphone,
        "companyemail": companyemail,
        "industry": industry,
        "companydescription": companydescription,
        "companyaddress":companyaddress,
        "usertype": usertype
      })
      .then((response) => {
        alert(response.data.message)
        this.setState((prevState, props) => ({
                  // users: studentListCopy,
                  id: prevState.id
                }));
      }, (error) => {
        console.log(error);
      });
  }

  editStudentSubmit(id, username, password, companyname, companyphone, companyemail, industry, companydescription, companyaddress) {
    
      axios.put(url + 'editemployer/' +id, {
        "username": username,
        "password": password,
        "companyname": companyname,
        "companyphone": companyphone,
        "companyemail": companyemail,
        "companydescription": companydescription,
        "companyaddress": companyaddress,
        "industry": industry
      })
      .then((response) => {
        alert(response.data.message);
      }, (error) => {
        console.log(error);
      });
  }

  deleteStudent(id) {
    let r = window.confirm("Do you want to delete this item");
    if (r === true) {
      let filteredStudentList = this.state.users.filter(
        x => x.id !== id
      );

      axios.put(url + 'deleteemployer/' + id)
        .then(response => {
          alert(response.data);
          this.setState((prevState, props) => ({
            users: filteredStudentList
         }));
        })
        .catch(error => {
          console.log(error);
      });
    }
  }

  render() {

    const { isLoading, users, error } = this.state;

    if (isLoading)
      return <div>Loading...</div>

    return (
      <React.Fragment>
        <br />
        <Row>
          <h1>Job lists <span style={{ marginLeft: '5px' }}><sub></sub></span></h1>
        </Row>
        <br />
        <Form.Group as={Row} md="2" controlId="exampleForm.ControlSelect1">
          <Form.Label >Show </Form.Label>
          <Col sm={2}><Form.Control size="sm" as="select" >
            <option>10</option>
            <option>20</option>
            <option>30</option>
            <option>40</option>
            <option>50</option>
          </Form.Control></Col>
          <Form.Label >Entries </Form.Label>
          <Col md={{ span: 2, offset: 1 }}>
            <ModalComponent addStudentSubmit = {this.addStudentSubmit} />
          </Col>
          <Col md={{ span: 2 }}><Form.Control size="sm" type="text" placeholder="Search" /></Col>
        </Form.Group>
        <div>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>ID</th>
                <th>username</th>
                {/* <th>password</th> */}
                <th>companyname</th>
                <th>companyphone</th>
                <th>companyemail</th>
                <th>companydescription</th>
                <th>companyaddress</th>
                <th>industry</th>
                <th>set</th>
              </tr>
            </thead>
            <StudentList
              studentList = {users}
              editStudentSubmit = {this.editStudentSubmit}
              deleteStudent={this.deleteStudent}
            />
          </Table>
          {/* add modal part */}

          
          <Row>
            Showing 1 to 10 of 52 Jobs
                    <Col md={{ span: 2, offset: 4 }}><Pagination>
              <Pagination.First />
              <Pagination.Prev />
              <Pagination.Item>{1}</Pagination.Item>
              <Pagination.Ellipsis />

              <Pagination.Item>{10}</Pagination.Item>
              <Pagination.Item>{11}</Pagination.Item>
              <Pagination.Item active>{12}</Pagination.Item>
              <Pagination.Item>{13}</Pagination.Item>
              <Pagination.Item disabled>{14}</Pagination.Item>

              <Pagination.Ellipsis />
              <Pagination.Item>{20}</Pagination.Item>
              <Pagination.Next />
              <Pagination.Last />
            </Pagination></Col></Row>
        </div>

      </React.Fragment>
    );
  }
}

ReactDOM.render(<Candidate />, document.getElementById("root"));