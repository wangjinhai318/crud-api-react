import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div>
        <footer className="main-footer">
          <div className="pull-right hidden-xs">
    </div>
          Copyright © 2019 <b>FYP2K19S3</b>. All rights
          reserved.
  </footer>
      </div>

    )
  }
}
