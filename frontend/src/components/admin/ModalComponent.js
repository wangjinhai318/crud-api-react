import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class ModalComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { modal: false, username: '', companyname:'', companyphone: '', companyemail: '', industry: '', companydescription: '', companyaddress: '', usertype: ''};
    this.toggle = this.toggle.bind(this);
    this.addStudentSubmit = this.addStudentSubmit.bind(this);
    
  }

  toggle(event) {
    event.preventDefault();
    this.setState({
      modal: !this.state.modal
    });
  }

  addStudentSubmit() {
    this.props.addStudentSubmit(
                                this.username.value,
                                this.password.value,
                                this.companyname.value, 
                                this.companyphone.value,
                                this.companyemail.value, 
                                this.industry.value, 
                                this.companydescription.value,
                                this.companyaddress.value, 
                                this.usertype.value)
    console.log(this.username);
  this.setState({
    modal: !this.state.modal
  });
}

  render() {
    return (

        <div>
            <a href="/" color="danger" onClick={this.toggle}>Add New</a>
            <Modal isOpen={this.state.modal}>
            <form >
              <ModalHeader>Add New Employer</ModalHeader>
              <ModalBody>
              <div className="row">
                  <div className="md-form form-sm col-md-12">
                    <label for="inputSMEx">username:</label>
                    <input className="form-control" id="inputSMEx" ref={username => this.username = username}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>password:</label>
                    <input type="password" className="form-control"  ref={password => this.password = password}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyname:</label>
                    <input className="form-control"  ref={companyname => this.companyname = companyname}/>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyphone:</label>
                    <input className="form-control" ref={companyphone => this.companyphone = companyphone} />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyemail:</label>
                    <input type="email" className="form-control" ref={companyemail => this.companyemail = companyemail} />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>industry:</label>
                    <input className="form-control" ref={industry => this.industry = industry} />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companydescription:</label>
                    <input className="form-control" ref={companydescription => this.companydescription = companydescription} />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>companyaddress:</label>
                    <input className="form-control" ref={companyaddress => this.companyaddress = companyaddress} />
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-md-12">
                    <label>usertype:</label>
                    <input className="form-control" ref={usertype => this.usertype = usertype} />
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <input type="button" onClick={this.addStudentSubmit} value="Save"  class="btn btn-secondary" />
                <Button class="btn btn-secondary"  onClick={this.toggle}>Cancel</Button>
              </ModalFooter>
              </form>
            </Modal>
        </div>
      
    );
  }
}
